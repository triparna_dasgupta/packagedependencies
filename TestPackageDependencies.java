import static org.junit.Assert.*;

import org.junit.Test;

public class TestPackageDependencies {
	
    /**
     * This method checks whether the program prints proper error messages when the file
     * from which data is to be read is not found in the location mentioned by the user as
     * input parameter.
     */
	@Test
	public void testFileDoesNotExist() {
		String filePath = "NoFile.txt";
		PackageDependencies.getDepMatrixFromFile(filePath);
		fail("Program allowed execution for a file that does not exist");
	}
	
	/**
     * This method checks whether the program prints proper error messages when a line in the file
     * being read for getting the direct dependencies of the packages, does not adhere to the pattern
     * decided.
     */
	@Test
	public void testTextPatternNotFollowed() {
		String filePath = this.getClass().getResource("PatternNotFollowed.txt").getPath();
		PackageDependencies.getDepMatrixFromFile(filePath);
		fail("Program allowed execution for a file that does not adhere to the pattern set for data");
	}
	
	/**
     * This method checks whether the program prints proper error messages when the number of arguments
     * given by user to run the program are less than two.
     */
	@Test
	public void testInvalidArgument() {
		String filePath = this.getClass().getResource("packages.txt").getPath();
		String[] args = new String[1];
		args[0] = filePath;
		PackageDependencies.main(args);
		fail("Program allowed execution for lesser number of arguments");
	}
}