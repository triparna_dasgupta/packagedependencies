

/**
 * This is the main class of the Package Dependencies Parser programme. 
 * The program will be run from the console with at least two parameters, the first of which 
 * is the fully qualified filename containing the direct dependencies. The other arguments are 
 * package names for which the dependency list is to be displayed
 * 
 * @author Triparna Dasgupta
 */

import java.util.TreeSet;
import java.util.regex.Pattern; // Regular expressions will be used to parse file format
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class PackageDependencies {
    /* Define pattern to describe each line in the dependencies file.
     * Assumptions:
     * Package names must start with a letter of the English alphabet.
     * Package names can be alphanumeric and contain . or _
     * Package names cannot have any white-spaces between letters, digits or characters in its name.
     * A line will be considered valid if the package name is valid and is followed by a -> 
     * (with the possibility of there being white spaces in between)
     */
    
   static Pattern pattern = Pattern.compile ("[a-zA-Z][a-zA-Z0-9\\._]*\\s*->.[[a-zA-Z][a-zA-Z0-9\\._]*\\s*]*");
   static ArrayList<String> processedPackages = new ArrayList<String>();
   
   /**
    * This method takes in the file name and returns a HashMap object containing 
    * the package name and their direct dependencies as a TreeSet object. If the file name 
    * does not exist or the file path is wrong then appropriate error message is displayed 
    * to the user. Also if there is any line in the file which does not adhere to the pattern
    * as declared in the object "pattern", appropriate error message will be displayed.
    * 
    * @param: The name of the file which contains the package names and its dependencies.
    * @return: A HashMap object containing the package names and their direct dependency list
    */
   public static HashMap<String, TreeSet<String>> getDepMatrixFromFile (String fileName) {
            BufferedReader reader = null;
            HashMap <String, TreeSet<String>> map = null;
            try {
                reader = new BufferedReader (new FileReader (fileName));
            } catch (FileNotFoundException fileNotFound) {
                System.err.println ("ERROR: File " + fileName + " not found");
                System.exit (2);         
            }
            
            try {
                if (reader != null) {
                    String line = null;
                    map = new HashMap <String, TreeSet<String>>();
                    while ((line = reader.readLine())!= null) {
                        //Kill leading or lagging white spaces
                        line = line.trim();
                        //Ignore blank lines in the file
                        if (line.equals("")) continue;
                        //If the lines do not adhere to the predefined format, report an error and exit
                        if (!pattern.matcher(line).matches()) {
                            System.err.println ("ERROR: File " + fileName + " contains invalid line");
                            System.err.println ("Could not parse: " + line);
                            System.exit (3);
                        }
                        
                        //Now build the data structures necessary to print the requested information
                        
                        TreeSet<String> dependencies = new TreeSet<> ();
                        String packageName, dependencyName;
                        
                        packageName = line.substring (0, line.indexOf ("->")).trim();
                        dependencyName = line.substring(line.indexOf("->") + 2).trim();
                        
                        StringTokenizer dependencyList = new StringTokenizer (dependencyName);
                        
                        while (dependencyList.hasMoreTokens()) {
                            dependencies.add (dependencyList.nextToken());
                        }
                        
                        //At this stage, an array should be built with the dependencies for the active package
                        //Add that to the HashMap
                        map.put(packageName, dependencies);
                    }
                }
            } catch (IOException ioex) {
                System.err.println ("ERROR: " + ioex.getMessage());
                System.exit (4);
            }
        return map;
   }
            
    /**
     * This is the main method which calls other methods in order to accept 
     * the file name as a parameter and then print as output the package names 
     * given as input parameters and their direct and transitive dependencies.
     * 
     * @param args must contain at least two arguments.
     * args[0] = file name containing the direct dependencies
     * args[1], args[2], ..., args[n] =&gt; package names
     */
    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println ("ERROR: Incorrect Usage.");
            System.err.println ("Usage: java PackageDependencies filename packagename1 [packagename2] ... [packagename n]");
            System.exit (1);
        }
        else {
            //Get the list of dependencies
            HashMap<String, TreeSet<String>> map = getDepMatrixFromFile(args[0]);
            
            //Now, for each package name provided, create the list of direct and transitive dependencies
            for (int i = 1; i < args.length; i++) {
                //Clear the store of dependencies for each package
                processedPackages.clear();
                TreeSet<String> allDeps = getAllDependencies (map, args[i]);   
                String output = args [i] + " ->";
                if (allDeps != null) {
                    for (String element : allDeps) {
                        if (!element.equals (args[i]))
                            output = output + " " + element;
                    }
                }
                System.out.println (output);
            }
        }
    }
    
    /**
     * This method takes in the HashMap containing all package names along with their 
     * direct dependency lists and the package name for which the dependencies have to
     * be shown and returns as output a TreeSet object which contains all the direct
     * and transitive dependencies of the given package name. This is a recursive method.
     * 
     * @param map: the object which contains the name of all packages and their direct 
     * dependencies
     * @param packageName: the name of the package for which the dependency list has to be displayed
     * @return Treeset<String>: The list of all the dependencies for the package name given as input
     */
    public static TreeSet<String> getAllDependencies (HashMap<String, TreeSet<String>> map, String packageName) {
        //Important to avoid circular references, that is, two packages depending on each other
        if (processedPackages.contains(packageName)) return null;
        //And to house-keep for potential circular references in the future.
        processedPackages.add (packageName);
        TreeSet<String> directDependencies = map.get(packageName);
        TreeSet<String> allDependencies = null; 
        if (directDependencies != null) {
            allDependencies = new TreeSet<String> (directDependencies);
            for (String str : directDependencies) {
                TreeSet<String> transitiveDependencies = getAllDependencies (map, str);

                if (transitiveDependencies != null)
                    allDependencies.addAll(transitiveDependencies);
            }
        }
        
        return allDependencies;
    }
}