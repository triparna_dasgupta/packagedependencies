There are two source code files submitted:
1) PackageDependencies.java: This file contains the actual program.
2) TestPackageDependencies.java: This is the Junit Test class for PackageDependencies.java and contains a few test cases to check whether program handles certain situations correctly or not.

The commands that need to be executed in order to run the program are as follows:
1) javac PackageDependencies.java
2) javac TestPackageDependencies.java (assuming that the Junit libraries are included in the classpath)

To run the Junit Tests:
"java TestPackageDependencies"

To run the actual program:
"java PackageDependencies <File Name> <Package Name1>... <Package NameN>"
 
An example:
"java PackageDependencies C:/packages.txt swingui gui unknown"

